# Presentations

Various presentations I have done over time.

## 2024

### Web Dev: State of the Art 2024

Slides [link](./2024/web_development-state-of-the-art-2024/presentation.odp)

![](./2024/web_development-state-of-the-art-2024/video.mp4)

### HOT Development Guidelines

![](./2024/hot_development_guidelines/video.mp4)
